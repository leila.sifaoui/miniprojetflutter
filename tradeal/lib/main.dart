import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:tradeal/routes.dart';
import 'package:tradeal/view/utils/constants.dart';
import 'package:tradeal/view/onboarding/splash_screen.dart';

Future<void> main()  async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(Tradeal());
}

class Tradeal extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Tradeal',
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
        fontFamily: "Made Tommy",
        textTheme: const TextTheme(
          bodyText1: TextStyle(color: kTextColor),
          bodyText2: TextStyle(color: kTextColor),
        ),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      //home: SplashScreen(),
      initialRoute: SplashScreen.routeName,
      routes: routes,
    );
  }
}
