import 'package:flutter/material.dart';
import 'package:tradeal/controller/auth.dart';

class SignIn extends StatefulWidget {
  const SignIn({Key? key}) : super(key: key);

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {

  final AuthController _auth = AuthController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.deepPurpleAccent[100],
      appBar: AppBar(
      backgroundColor: Colors.deepPurpleAccent[200],
      elevation: 0.0,
      title: Text('Sign In'),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
        child: RaisedButton(
          child: Text('Guest'),
          onPressed: () async {
            dynamic result = await _auth.signInAnon();
              if (result == null){
                print('error signing in');
              } else {
                print('signed in');
                print(result.uid);
              }
          }
        ),
      )
    );
  }
}
