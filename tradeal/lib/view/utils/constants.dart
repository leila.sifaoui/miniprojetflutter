import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xff966be8);
const kPrimaryLightColor = Color(0xffccb8f3);
const kPrimaryGradientColor = LinearGradient(
  begin: Alignment.topCenter,
  end: Alignment.bottomCenter,
  colors: [Color(0xff0cf2b4), Color(0xff966be8)],
);
const kSecondaryColor = Color(0xff0cf2b4);
const kSecondaryLightColor = Color(0xff6ef1bb);
const kTextColor = Color(0xff3d3f5b);

const kAnimationDuration = Duration(milliseconds: 200);