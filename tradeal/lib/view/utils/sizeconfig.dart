import 'package:flutter/material.dart';

class SizeConfig {
  static late MediaQueryData _mediaQueryData;
  static late double screenWidth;
  static late double screenHeight;
  static late double defaultSize;
  static late Orientation orientation;

  void init(BuildContext context){
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    orientation = _mediaQueryData.orientation;
  }
}

// dynamic height per screen size
double getProportionateScreenHeight(double inputHeight){
  double screenHeight = SizeConfig.screenHeight;
  //812 is the layout height that we're using
  return (inputHeight / 812.0) * screenHeight;
}

// dynamic width per screen size
double getProportionateScreenWidth(double inputWidth){
  double screenWidth = SizeConfig.screenHeight;
  //812 is the layout width that we're using
  return (inputWidth / 812.0) * screenWidth;
}