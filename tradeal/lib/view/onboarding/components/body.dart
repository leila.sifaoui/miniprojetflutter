import 'package:flutter/material.dart';
import 'package:tradeal/view/onboarding/components/splash_content.dart';
import '../../utils/constants.dart';
import '../../utils/sizeconfig.dart';
import 'default_button.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  int currentPage = 0;
  List<Map<String, String>> splashData = [
    {
      "image": "assets/images/testsplash2.png",
      "text": "Make a deal and save your money"
    },
    {
      "image": "assets/images/testsplash1.png",
      "text": "Stop buying, Start trading"
    },
    {
      "image": "assets/images/testsplash.png",
      "text": "Welcome to Tradeal"
    },
  ];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Column(
        children: <Widget>[
        Expanded(
          flex: 3,
          child: PageView.builder(
            onPageChanged: (value){
              setState((){
                currentPage = value;
              });
          },
            itemCount: splashData.length,
            itemBuilder: (context, index) => SplashContent(
              image: splashData[index]["image"],
              text: splashData[index]["text"],
          ),
         ),
        ),
        Expanded(
          flex: 2,
          child: Padding(
          padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Column(
            children: <Widget>[
              Spacer(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: List.generate(
                  splashData.length,
                  (index) => buildDot(index: index),
                ),
              ),
              Spacer(flex: 3),
              DefaultButton(
                text: "Continue",
                press: () {}
              ),
              Spacer(),
            ],
          ),
        ),
       ),
      ],
     ),
    ),
   );
  }

AnimatedContainer buildDot({int? index}) {
    return AnimatedContainer(
      duration: kAnimationDuration,
      margin: EdgeInsets.only(right: 5),
      height: 6,
      width: currentPage == index ? 20 : 6,
      decoration: BoxDecoration(
        color: currentPage == index ? kPrimaryColor : Color(0xffccb8f3),
        borderRadius: BorderRadius.circular(3),
      ),
    );
  }
}




