import 'package:firebase_auth/firebase_auth.dart';
import 'package:tradeal/model/user.dart';

class AuthController {

  final FirebaseAuth _auth = FirebaseAuth.instance;

  // Create User Obj Based on FirebaseUser
  Member? _userFromUser(User user){
    return user != null ? Member(user.uid) : null;
  }

  //Auth Stream (Session)
  Stream<Member?> get user {
    return _auth.authStateChanges()
        .map((User? user) => _userFromUser(user!));
  }

  //Sign In Anon
  Future signInAnon() async {
    try {
      UserCredential result = await _auth.signInAnonymously();
      User? user = result.user;
      return _userFromUser(user!);
    } catch(e) {
      print(e.toString());
      return null;
    }
  }
}