import 'package:flutter/material.dart';
import 'package:tradeal/view/onboarding/splash_screen.dart';

final Map<String, WidgetBuilder> routes = {
  SplashScreen.routeName: (context) => SplashScreen(),
};