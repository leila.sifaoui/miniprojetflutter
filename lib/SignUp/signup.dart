import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:tradeal/Login/login.dart';
import 'package:tradeal/Welcome/welcome.dart';
import 'package:tradeal/constants.dart';
import 'package:tradeal/home.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  late String _username;
  late String _email;
  late String _password;
  final auth = FirebaseAuth.instance;

  final GlobalKey<FormState> _keyForm = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(color: Colors.white,
          onPressed: () {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => Welcome()));
          },
        ),
        centerTitle: true,
        backgroundColor: kPrimaryColor,
        title: const Text('SIGN UP'),
      ),
      body: Form(
        key: _keyForm,
        child: ListView(

          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.fromLTRB(0, 30, 0, 30),
                  child: Image.asset(
                    "assets/images/Logo.png",
                    width: size.width * 0.25,
                  ),
                ),
              ],
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(20, 0, 20, 20),
              child: TextFormField(
                decoration: const InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: kPrimaryColor),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: kPrimaryColor),
                    ),
                    labelText: "Username",
                    labelStyle: TextStyle(
                      color: kPrimaryColor,
                    )),
                onSaved: (String? value) {
                  _username = value!;
                },
                validator: (String? value) {
                  if (value == null || value.isEmpty) {
                    return "Field is empty";
                  } else if (value.length < 5) {
                    return "Minimum of 5 characters required";
                  } else {
                    return null;
                  }
                },
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(20, 0, 20, 20),
              child: TextFormField(
                decoration: const InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: kPrimaryColor),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: kPrimaryColor),
                    ),
                    labelText: "Email",
                    labelStyle: TextStyle(
                      color: kPrimaryColor,
                    )),
                onSaved: (String? value) {
                  _email = value!;
                },
                validator: (String? value) {
                  String pattern =
                      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
                  if (value == null || value.isEmpty) {
                    return "Field is empty";
                  } else if (!RegExp(pattern).hasMatch(value)) {
                    return "Email is incorrect";
                  } else {
                    return null;
                  }
                },
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(20, 0, 20, 20),
              child: TextFormField(
                obscureText: true,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: kPrimaryColor),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: kPrimaryColor),
                    ),
                    labelText: "Password",
                    labelStyle: TextStyle(
                      color: kPrimaryColor,
                    )),
                onSaved: (String? value) {
                  _password = value!;
                },
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Password is empty";
                  } else if (value.length < 8) {
                    return "Minimum of 8 characters required";
                  } else {
                    return null;
                  }
                },
              ),
            ),

            Container(
              margin: const EdgeInsets.fromLTRB(10, 20, 10, 0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(29),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 40),
                    primary: kPrimaryLightColor,
                  ),
                  onPressed: () {
                    if(_keyForm.currentState!.validate()) {
                      _keyForm.currentState!.save();
                      auth.createUserWithEmailAndPassword(
                          email: _username, password: _password).then((_) {
                        Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (context) => HomePage()));
                      });
                    }
                  },
                  child: const Text(
                    "SIGN UP",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16),
                  ),
                ),
              ),
            ),

            Container(
              margin: const EdgeInsets.fromLTRB(10, 20, 10, 20),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(29),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 40),
                    primary: kPrimaryColor,
                  ),
                  onPressed: () {
                    Navigator.of(context).pushReplacement( MaterialPageRoute(builder: (context) => const Login()));
                  },
                  child: const Text(
                    "LOGIN",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16),
                  ),
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }
}
