import 'package:flutter/material.dart';
import 'package:tradeal/Login/login.dart';
import 'package:tradeal/SignUp/signup.dart';
import 'package:tradeal/Welcome/components/background.dart';
import 'package:tradeal/constants.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(

      child: Column(

        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            padding: const EdgeInsets.fromLTRB(0, 280, 0, 130),
            child: Image.asset(
              "assets/images/Logo.png",
              width: size.width * 0.4,
            ),
          ),
          SizedBox(
            width: size.width * 0.8,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(29),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  padding:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 40),
                  primary: kPrimaryLightColor,
                ),
                onPressed: () {
                  Navigator.of(context).pushReplacement( MaterialPageRoute(builder: (context) => const Login()));
                },
                child: const Text(
                  "LOGIN",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 16),
                ),
              ),
            ),
          ),
          const SizedBox(height: 50),
          SizedBox(
            width: size.width * 0.8,
            child: ClipRRect(
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: Colors.transparent,
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 40),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(29),
                    ),
                    side: const BorderSide(
                      width: 3,
                      color: Colors.white,
                    )),
                onPressed: () {
                  Navigator.of(context).pushReplacement( MaterialPageRoute(builder: (context) => const SignUp()));
                },
                child: const Text(
                  "SIGN UP",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 16),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
